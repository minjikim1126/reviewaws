package com.chicken.review.login.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;

import com.chicken.review.login.service.AwsS3Service;
import com.chicken.review.login.service.ReviewService;
import com.chicken.review.login.vo.ReviewVO;

@Controller
public class UserController {
	
	private final Logger logger = LoggerFactory.getLogger(UserController.class);
	
	
	@Autowired
    private ReviewService reviewService;
	@Autowired
	private AwsS3Service awsService;
	
	String user = "Carry";

    @RequestMapping(value="/login")
    public String login(Model model, String error, String logout) {
    	
    	ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
    	HttpServletRequest request = attr.getRequest();
    	HttpServletResponse response = attr.getResponse();

    	Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    	
    	model.addAttribute("reviewList", reviewService.getReviewList());
    	model.addAttribute("user", user);
    	
    	System.out.println(reviewService.getReviewList());
    	if(authentication != null) {
    		
	    	if(!authentication.getPrincipal().equals("anonymousUser"))
	    		return "redirect:/";
    	}
    	
        return "login/login";
    }

    @PostMapping("/fileUpload")
    @ResponseBody
    public int fileUpload(@RequestParam("mediaFile") MultipartFile file,
    		@RequestParam("seq") int seq,
    		@RequestParam("title") String title,
    		@RequestParam("content") String content,
    		Model model) throws IllegalStateException, IOException{
/*   
       Integer paramSeq = seq;
       if(paramSeq == null) {
    	   paramSeq = 0;
       }
*/
	   System.out.println("title: " + title);
	   System.out.println("content: " + content);
	   
	   String userId = "carry1126";
	   ReviewVO reviewVO = new ReviewVO();
	   reviewVO.setSeq(seq);
	   reviewVO.setTitle(title);
	   reviewVO.setContent(content);
	   reviewVO.setUserId(userId);
	   int result = awsService.s3FileUpload(file, reviewVO);
	   
	   System.out.println("Insert result : " + result);

    	return result;
    }
    
    
    @GetMapping("/deleteInfo")
    public String deleteInfo(@RequestParam("seq") int seq)  throws IllegalStateException, IOException{
    	
    	System.out.println("delete seq:" + seq);
    	reviewService.deleteReview(seq);
    	
    	return "redirect:/";
    }
    
    @GetMapping("/modifyInfo")
    @ResponseBody
    public  Map modifyInfo(@RequestParam("seq") int seq) throws IllegalStateException, IOException{
     //public  ModelAndView modifyInfo(@RequestParam("seq") int seq) throws IllegalStateException, IOException{
     //public  ModelAndView modifyInfo(HttpServletRequest request, @RequestParam("json") String data) throws IllegalStateException, IOException, org.json.simple.parser.ParseException{
     /*
    	JSONParser parser = new JSONParser();
    	JSONObject obj = null;
    	obj = (JSONObject)parser.parse(data);
      */	
    	
    	System.out.println(seq);
    	//ModelAndView mv = new ModelAndView();
    	//ReviewVO reviewInfo = reviewService.getReviewInfo((int)obj.get("seq"));
    	ReviewVO reviewInfo = reviewService.getReviewInfo(seq);
    /*	
    	mv.setViewName("login/modify");
    	mv.addObject("reviewList", reviewService.getReviewList());
    	mv.addObject("user", user);
    	mv.addObject("firstYN", "n");
    	mv.addObject("reviewInfo", reviewInfo);
    	
    	return mv;
     */
    	
      Map<String, Object> result = new HashMap();
      result.put("reviewInfo", reviewInfo);
      
      return result;
    }
   
}

package com.chicken.review.login.service;

import java.io.File;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.chicken.review.login.mapper.ReviewMapper;
import com.chicken.review.login.vo.ReviewVO;

@Service
public class AwsS3Service {
	private static final String BUCKET_NAME="reviewaws";  //S3 버킷명
	//AWS IAM에서 확인 :그룹 -> 사용자 추가
	private static final String ACCESS_KEY = "AKIA4UQODBQPJAY2NN76";
	private static final String SECRET_KEY = "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";
	
	private AmazonS3 s3;
	
	@Autowired
	private ReviewMapper reviewMapper;
	
	public AwsS3Service() {
		AWSCredentials awsCredentials = new BasicAWSCredentials(ACCESS_KEY, SECRET_KEY);
		
		s3 = AmazonS3Client.builder()
					.withRegion(Regions.AP_NORTHEAST_2) /*서울*/
					.withCredentials(new AWSStaticCredentialsProvider(awsCredentials)).build();
	}

	public int s3FileUpload(MultipartFile file, ReviewVO reviewVO) throws IllegalStateException, IOException{
		int result;
		//임시로 로컬파일경로에 저장 => 실제 s3로 전송
		if(!file.getOriginalFilename().contentEquals("")) {
			//File localFile = new File("C:\\\\Users\\minji\\Downloads\\" + file.getOriginalFilename()); //윈도우 역슬래시, 맥 슬래시 [로컬]경로
			File localFile = new File("/usr/local/tomcat/temp" + file.getOriginalFilename()); //[서버]경로
			file.transferTo(localFile);
			
			System.out.println(localFile.getName());
			PutObjectRequest obj = new PutObjectRequest(BUCKET_NAME, localFile.getName(), localFile);
			obj.setCannedAcl(CannedAccessControlList.PublicRead);
			s3.putObject(obj);
			String imageUrl = "https://reviewaws.s3.ap-northeast-2.amazonaws.com/" + localFile.getName();
			reviewVO.setS3ImageUrl(imageUrl);
		}else {
			reviewVO.setS3ImageUrl("");
		}
		int seq = reviewVO.getSeq();
		if(seq == 0) {
			result = reviewMapper.insertReview(reviewVO);
		}else {
			result = reviewMapper.UpdateReview(reviewVO);
		}
		
		return result;
	}

}

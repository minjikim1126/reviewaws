package com.chicken.review.login.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.web.bind.annotation.RequestParam;

import com.chicken.review.login.vo.ReviewVO;


@Mapper
public interface ReviewMapper {

	List<ReviewVO> getReviewList();
	int insertReview(ReviewVO review);
	boolean deleteReview(@RequestParam("seq") int seq);
	ReviewVO getReviewInfo(@RequestParam("seq") int seq);
	int UpdateReview(ReviewVO reviewVO);
	
}

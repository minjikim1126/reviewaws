package com.chicken.review.login.vo;

import lombok.Data;

@Data
public class UserVO {

	String id;
	String email;
	String nickname;
	
}

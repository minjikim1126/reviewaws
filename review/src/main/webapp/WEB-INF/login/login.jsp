<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<script src="//developers.kakao.com/sdk/js/kakao.min.js"></script>

<style>
	
	
	.loginWrapper{
		margin-top: 50px;
		margin-left:auto;
		margin-right:auto;
		max-width: 400px;
		height: 250px;
		border-radius:5px;
		text-align: center;
		line-height:1.8;
	}
	
	#kakao-login-btn{
		margin-top:20px;
    	border-top: 1px solid #DDDDDD99;
	}
	.itemListWrapper{
		width: 960px;
		height: 1500px;
		margin: 100px auto;
		text-align: center;
	}
	.itemList{
		float:left;
		width: 306px;
		height: 450px;
		margin: 5px;
		border-radius: 5px;
		border : 2px solid #DDDDDD;
	}
	.imageArea{
		width: 100%;
		height: 200px;
		background: #EEEEEE;
		overflow:hidden;
	}
	
	.imageArea > img{
		width: 100%;
		height: 100%;
	}
	.reviewArea{
		width: 100%;
		height: 180px;
		text-align: left;
		padding : 4px;
	}
	.reviewTitle{
		width: 100%;
		height: 20px;
		text-align: left;
		padding : 4px;
	}
	
	.reviewArea > textarea {
		resize: none;
		width: 98%;
		height: 168px;
		background:#AAAA0011;
		overflow:hidden;
    	border: 0px !important;
	}
	.slide-child{
		transform: translateY(50px);
        opacity: 0;
        transition: all 1s;
    }
    .is-visible{
		transform: translateY(0px);
        opacity: 1;
    }
    .write-btn{
    	width: 100px;
    	height:  28px;
    	margin: 0px auto;
    	background: linear-gradient(to right, #C02425, #F0CB35);
    	color:white;
    	border-radius: 5px;
    	opacity: 0.8;
    	cursor:pointer;
    	padding-top:0px;
    	margin-top:20px;
    	border-top:  1px solid #DDDDDD99;
    }
    .write-btn:hover{
    	opacity: 1;
    }
    
    .edit-btn{
    	float:right;
    	padding-right: 10px;
    }
    
    .cover-form{
		width: 350px;
	    height: 500px;
	    background: white;
	    position: fixed;
	    z-index: 10;
	    border-radius: 10px;
    }
    
   .form-title{
   		font-size:	25px;
   		font-weight: bold;
   		width: 100%;
   		text-align: center;
   		padding: 10px;
   }
   .form-desc{
   		font-size:	14px;
   		text-align: left;
   		padding: 20px;
   }
   
   .input-title{
   		width: 150px;
   		height: 50px;
   		text-align: left;
   		font-size: 14px;
   }
   .input-content{
   		width: 250px;
   		height: 200px;
   }
</style>
<script type="text/javascript" src="js/ajaxUtil.js"></script>
<script type="text/javascript">

$(document).ready(function(){
	$("#cover-form").hide();
    
    var slideAelements = $('.slide-child')
    
    
    function animateSlideA() {
      slideAelements.each(function (i) {
          setTimeout(function () {
              slideAelements.eq(i).addClass('is-visible');
          }, 300 * (i + 1));
      });
    }
    animateSlideA() ;
    
    $("#write-btn").click(function(){
    	$("#cover-form").show();
    });
    
    $("#cancelBtn").click(function(){
    	$("#cover-form").hide();
    });
    
    //글쓰기저장버튼 이벤트처리(ajax)
    $("#btnSubmit").click(function(event){
    	
    	//preventDefault는 submit을 막음
    	event.preventDefault();
    	
    	var form = $('#fileUploadForm')[0];
    	
    	var data = new FormData(form);
    	
    	$("#btnSubmit").prop("disabled", true);
    	
    	$.ajax({
    		  type: "POST",
    		  enctype: 'multipart/form-data',
    		  url: "/fileUpload",
    		  data: data,
    		  processData: false,
    		  contentType: false,
    		  cache: false,
    		  timeout: 600000,
    		  success:function(data){
    			  alert("write complete");
    			  $("#btnSubmit").prop("disabled", false);
    			  $("#cover-form").hide();
    			  location.reload();
    		  },
    		  error:function(e){
    			  console.log("ERROR:", e);
    			  $("#btnSubmit").prop("disabled", false);
    			  alert("fail");
    			  $("#cover-form").hide();
    			  location.reload();
    		  }
    	});
    });
      
});

	//글삭제 기능처리
	function delInfo(seq){
		var chk  = confirm("정말 삭제하시겠습니까?");
		if(chk){
			window.location.href='/deleteInfo?seq='+seq;
		}
	}
	
	//글수정 기능처리
	function modifyInfo(seq){
		alert("수정합니다.");
		/*
		var obj = new Object();
		obj.seq = seq;
		var jsonData = JSON.stringify(obj);
		*/
		sendIt(seq);

    }
	
	function sendIt(seq){
		//XMLHttpRequest 객체를 생성
		xmlHttp = createXMLHttpRequest();
		// 1.브라우저 검색 2. 그에 맞게 xmlReq 객체 생성 후 여기에 반환
		
		var query="";
		
		// get방식 데이터 전송
		query="/modifyInfo?seq="+seq;
		
		xmlHttp.onreadystatechange = callback;
		// onreadystatechange라는 프로퍼티(메소드)는
		// 서버가 작업을 마치고 클라이언트한테 
		// 정보나 데이터를 되돌려 줄 때 자동으로 실행되는 메소드
		// 콜백메소드(자동으로 실행되는 함수)명은 사용자 정의
		
		//서버한테 보내기
		xmlHttp.open("GET", query, true);
		//위에 함수가 get방식이므로
		//true: 비동기방식으로 보낸다 (서버에 a작업던져놓고,  나는 딴 짓하고 있다. 대신 받아주는 함수 필요 => callback)
		//ajax방식은 대부분 true
		xmlHttp.send(null);
		// POST 방식으로 보낼때는 uri에 줄줄이 데이터를 못붙이므로
		// null자리에다가 써주면 됨
		//서버에서 처리작업 맡긴거임.
		
	}

	function callback(){
		if(xmlHttp.readyState==4) { // ppt 참고 
			if(xmlHttp.status==200) {
				printData();
			}
		}
	}
	
	function printData(){
		//받아낸 XML(responseXML)에 root라는 애가 있음
		//var result = xmlHttp.responseXML.getElementsByTagName("root")[0];
		//Tag오면 Element's'
		//Id오면 Element
		
		//var data = result.firstChild.nodeValue;  // root의 첫번째 value값
		//alert(data);
		debugger;
       /*		
		console.log(xmlHttp.responseText);
		console.log(xmlHttp.response);
	   */
		document.getElementById("cover-form").style.display='block'; //show('block') hidden('none') 
		responseObject = JSON.parse(xmlHttp.responseText).reviewInfo;
		//console.log("responseObject: " + responseObject);
		/*
		alert("결과 값 : " + responseObject);
		alert("seq: " + responseObject.seq);
		alert("title: " + responseObject.title);
		alert("content: " + responseObject.content);
		alert("s3ImageUrl: " + responseObject.s3ImageUrl);
		*/
		//내용을 세팅
		var imageUrl = responseObject.s3ImageUrl.split('/');
		document.getElementById("seq").value=responseObject.seq;
		document.getElementById("title").value=responseObject.title;
		document.getElementById("content").value=responseObject.content;
		document.getElementById("imgUrl").innerHTML='<a href="javascript:'+responseObject.s3ImageUrl+'">'+ imageUrl[3] +'</a>';
		
	}
</script>

<html>
<title>오프라인 리뷰 웹테스트</title>
<body>

<div class="loginWrapper">
	<div>
		<div class="slide-child">OffREV 는 Offline</div>
		<div class="slide-child">Review Flatform 의 약자로써</div>
		<div class="slide-child">오프라인 후기 정보들을</div>
		<div class="slide-child">모아모아 제공합니다.</div>
	</div>

	<div id="cover-form" class="cover-form">
		<div class="form-title">리뷰 쓰기</div>
		<div class="form-desc">오프라인 행사 리뷰를 작성해주세요.</div>

		<form method="POST" action="/fileUpload" enctype="multipart/form_data" id="fileUploadForm">
		<div id="addSeq"></div>
			      <input type=hidden id="seq" name="seq" value="0" >
				  제목<input type=text id="title" name="title" class="input-title"><br/>
				  내용<br/>
				  <textarea id="content" name="content" class="input-content">	  
				  </textarea>		  
				  <input type=file id="mediaFile" name="mediaFile" ><br/>
				  <div id="imgUrl"></div>
			  <input type="submit" value="저장하기" id="btnSubmit" />
			  <input type="button" value="취소하기" id="cancelBtn" />
		</form>
	</div>
	
	<div id="write-btn" class="write-btn">
		글쓰기
	</div>
	
</div>

<div class="itemListWrapper">
<c:forEach var="item" items="${reviewList}" varStatus="status">
	<div class="itemList slide-child">
   	   <div class="imageArea"><img src="<c:out value="${ item.s3ImageUrl }" />"/></div>
	   <div class="reviewArea">
	   	  <div class="reviewTitle" ><c:out value="${ item.title }" /> </div>
	      <textarea readonly><c:out value="${ item.content }" /></textarea>
		  <button onclick="modifyInfo(${item.seq})">수정</button>
		  <button onclick="delInfo(${item.seq})">삭제</button>
	   </div>
    </div>
</c:forEach>
 </div>

</body>
</html>
